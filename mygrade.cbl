       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. SUWIGAK.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE   VALUE HIGH-VALUE .       
           05 COU-ID      PIC 9(6)V9(3).
           05 COU-NAME    PIC X(47).
           05 GRADE       PIC X(3).
       FD  AVG-FILE.
       01  AVG-DETAIL.
           05 COU-ID   PIC 9(6)V9(3).
           05 GRADE PIC X(3).

       PROCEDURE DIVISION .
       000-BEGIN.
           DISPLAY "=============AVG-GRADE====================="
           OPEN INPUT GRADE-FILE
           OPEN OUTPUT AVG-FILE 
           PERFORM UNTIL END-OF-GRADE-FILE
              READ GRADE-FILE
                 AT END SET END-OF-GRADE-FILE TO TRUE 
              END-READ 
              IF NOT END-OF-GRADE-FILE THEN            
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF               
           END-PERFORM 

           CLOSE GRADE-FILE
           CLOSE AVG-FILE 
           GOBACK 
           .
       001-PROCESS. 
           MOVE COU-ID IN GRADE-DETAIL  TO COU-ID IN AVG-DETAIL 
           MOVE GRADE IN GRADE-DETAIL  TO GRADE IN AVG-DETAIL     
           DISPLAY COU-ID IN GRADE-DETAIL "  " GRADE IN GRADE-DETAIL
           WRITE AVG-DETAIL
           . 
       001-EXIT.
           EXIT 
       .                  


       
